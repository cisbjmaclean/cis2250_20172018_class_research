package com.example.brett.androidroomwithrecyclerview;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by brett on 2018-02-01.
 */

public class CreateInput extends AppCompatActivity {

    private static final String TAG = "CreateInput";

    EditText firstName;
    EditText lastName;
    EditText email;
    Button saveButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_input);

        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        saveButton = findViewById(R.id.save_button);

        final AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                .allowMainThreadQueries()
                .build();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: firstName: " + firstName.getText().toString());
                db.infoDao().insertAll(new Info(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString()));
                startActivity(new Intent(CreateInput.this, RoomExample.class));
            }
        });
    }
}
