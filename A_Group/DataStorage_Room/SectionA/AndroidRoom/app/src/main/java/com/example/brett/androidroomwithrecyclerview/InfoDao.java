package com.example.brett.androidroomwithrecyclerview;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by brett on 2018-02-02.
 */

@Dao
public interface InfoDao {
    @Query("SELECT * FROM info")
    List<Info> getAllUsers();

    @Insert
    void insertAll(Info... infoList);
}
