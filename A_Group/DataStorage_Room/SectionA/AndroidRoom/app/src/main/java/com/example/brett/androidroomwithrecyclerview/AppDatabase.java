package com.example.brett.androidroomwithrecyclerview;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by brett on 2018-02-02.
 */

@Database(entities = {Info.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract InfoDao infoDao();
}