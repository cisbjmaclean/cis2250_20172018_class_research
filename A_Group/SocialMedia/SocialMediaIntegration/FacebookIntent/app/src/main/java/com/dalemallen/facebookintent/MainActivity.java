package com.dalemallen.facebookintent;
/*
* author: Dale Allen
* date: 2/1/2018
* purpose: using intent to open facebook
 */
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    // Created Variables
    TextView textViewMessage;
    EditText editTextMessage;
    Button buttonFB;
    Button buttonTest;
    Button buttonShare;
    // Create intent
    Intent intent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // linked widgets
        textViewMessage = findViewById(R.id.textViewMessage);
        editTextMessage = findViewById(R.id.editTextMessage);
        buttonFB = findViewById(R.id.buttonFB);
        buttonTest = findViewById(R.id.buttonTest);
        buttonShare = findViewById(R.id.buttonShare);


        buttonFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    // To open a web page, use the ACTION_VIEW action and specify the web URL in the intent data
                    textViewMessage.setText("Success!");
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/"));
                    // then start the intent
                    startActivity(intent);
                } catch(Exception e) {
                    //If that page is not found try this
                    textViewMessage.setText("Broken i think?");
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.help.com/")));
                }
            }
        });

        // This button is just a test
        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    // tries to open a page and set a message
                    textViewMessage.setText("I think it worked!");

                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=dQw4w9WgXcQ"));

                    // then start the intent
                    startActivity(intent);
                } catch(Exception e) {
                    textViewMessage.setText("Broke this one also?");
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=d5ab8BOu4LE")));
                }
            }
        });

        buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // create a string variable and get the text from the editText
                String message = editTextMessage.getText().toString();
                // is sending text content from one activity to another
                intent = new Intent(Intent.ACTION_SEND);
                // setting
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"Testing Intent");
                intent.putExtra(Intent.EXTRA_TEXT,message);
                //createChooser is a dialog create when an action can result in more than one outcome
                startActivity(intent.createChooser(intent,"Share via"));

            }
        });
    }
}
