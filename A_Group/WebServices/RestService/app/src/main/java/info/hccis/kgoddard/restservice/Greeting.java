package info.hccis.kgoddard.restservice;

/**
 * Created by kgoddard on 1/19/2018.
 */

public class Greeting {

    private String id;
    private String content;

    public String getId() {
        return this.id;
    }

    public String getContent() {
        return this.content;
    }

}