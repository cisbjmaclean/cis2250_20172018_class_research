package info.hccis2250.kotlindemotwo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class KotlinMain : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_main)
        val helloWorldText = findViewById<TextView>(R.id.KotlinText)
        helloWorldText.text = "Hello to a new world"
    }
}
