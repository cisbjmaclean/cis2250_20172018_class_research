package com.example.owner.picturepip;

import android.app.PictureInPictureParams;
import android.content.res.Configuration;
import android.graphics.Point;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Rational;
import android.view.Display;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar = getSupportActionBar();

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //This is used to get the display size
                Display display = getWindowManager().getDefaultDisplay();
                //This will hold the X and Y coordinates (int)
                Point size = new Point();
                display.getSize(size);
                //Putting the coordinates into the variables
                int width = size.x;
                int height = size.y;

                //Rationalizes and makes it smaller ---- 2/4 == 1/2 basically a fraction
                Rational aspectRatio = new Rational(width, height);

                //Builds the Picture in Picture Mode
                PictureInPictureParams.Builder mPictureInPictureParamsBuilder =
                        new PictureInPictureParams.Builder();

                //Puts the aspect into the builder
                mPictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build();

                //Enters the picture in picture mode
                enterPictureInPictureMode(mPictureInPictureParamsBuilder.build());

            }
        });
    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode, Configuration newConfig) {
        //OPTIONAL - This is used if you want the action bar removed when in picture/picture mode
        if(isInPictureInPictureMode){
            actionBar.hide();
        }else{
            actionBar.show();
        }
    }
}
