
### What is this repository for? ###

* This repository will contain the details about each reseach project for this year's CIS2250.


###  Submission Expectations ###

* folder name:  cis2250_research_lastfirst1_lastfirst2 where lastfirst1/2 are group member names in alphabetical order.
* Link to online recording of research to be provided prior to due data.
* PDF document to be provided containing a summary / instructions for users.
* Any code to be provided in a subfolder /code

### Who do I talk to? ###

* BJ MacLean 