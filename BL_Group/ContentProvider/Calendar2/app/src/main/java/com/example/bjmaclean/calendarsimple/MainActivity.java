package com.example.bjmaclean.calendarsimple;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void addEvent(View view) {
        //Define start time in milliseconds
        long startMillis = 0;
        //Define end time in milliseconds
        long endMillis = 0;
        //Create an instance for setting the beginning time of the calendar
        Calendar beginTime = Calendar.getInstance();
        //Pass the parameters to set beginning time
        beginTime.set(2017, 04, 27, 8, 15);
        //Get begin date in milliseconds after calculation
        startMillis = beginTime.getTimeInMillis();
        //Create an instance for setting the end date of the event
        Calendar endTime = Calendar.getInstance();
        //Pass the parameters to set end date of the event
        endTime.set(2017, 04, 27, 9, 15);
        //Get end date in milliseconds after calculation
        endMillis = endTime.getTimeInMillis();
        //Create content resolver object to insert data
        ContentResolver cr = getContentResolver();
        //Create content values object to insert data
        ContentValues values = new ContentValues();
        //Get default time zone
        TimeZone timeZone = TimeZone.getDefault();
        Log.d("info", "The default time zone is  " + timeZone);
        //Insert the data into the calendar
        values.put(CalendarContract.Events.DTSTART, startMillis); //Start date
        values.put(CalendarContract.Events.DTEND, endMillis); //End date
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID()); //Time zone ID
        values.put(CalendarContract.Events.TITLE, "My Simple Calendar"); //Title of the event
        values.put(CalendarContract.Events.DESCRIPTION,"Add only one event"); //Description of the event
        values.put(CalendarContract.Events.CALENDAR_ID, 1);//Set the Id of the event
//        values.put(CalendarContract.Events.ALL_DAY, 1);
        values.put(CalendarContract.Events.HAS_ALARM, 1); //Configure alarm

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // Retrieve ID for new event
        String eventID = uri.getLastPathSegment();
        Log.d("info","The event was added with id "+eventID+"in calendar ");
        Toast.makeText(getBaseContext(), "New event inserted", Toast.LENGTH_LONG)
                .show();


    }
}
