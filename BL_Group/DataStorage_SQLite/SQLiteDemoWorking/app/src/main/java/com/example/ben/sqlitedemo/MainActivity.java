package com.example.ben.sqlitedemo;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper myDB;
    EditText editTextFirstName;
    EditText editTextLastName;
    Button buttonInsert;
    Button buttonView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDB = new DatabaseHelper(this);

        editTextFirstName = (EditText)findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText)findViewById(R.id.editTextLastName);
        buttonInsert = (Button)findViewById(R.id.buttonInsert);
        buttonView = (Button)findViewById(R.id.buttonView);
        addData();
        viewAll();
    }

    public void addData() {
        buttonInsert.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(editTextLastName.getText().toString().isEmpty() || editTextFirstName.getText().toString().isEmpty()) {
                            Toast.makeText(MainActivity.this, "Data Not Inserted", Toast.LENGTH_LONG).show();
                        } else {
                            myDB.insertData(editTextFirstName.getText().toString(),
                                    editTextLastName.getText().toString());
                            Toast.makeText(MainActivity.this, "Data Inserted", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    public void viewAll() {
        buttonView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Cursor result = myDB.getAllData();

                        StringBuffer buffer = new StringBuffer();
                        while(result.moveToNext()) {
                            buffer.append("ID: " + result.getString(0)+ "\n");
                            buffer.append("First Name: " + result.getString(1)+ "\n");
                            buffer.append("Last Name: " + result.getString(2)+ "\n\n");
                        }

                        showMessage("Data", buffer.toString());
                    }
                }
        );
    }

    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


}
