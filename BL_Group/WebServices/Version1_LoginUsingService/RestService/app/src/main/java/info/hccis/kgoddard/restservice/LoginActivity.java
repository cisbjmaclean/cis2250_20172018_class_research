package info.hccis.kgoddard.restservice;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                new HttpRequestTask().execute();
            }
        });
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, User> {
        @Override
        protected User doInBackground(Void... params) {
            try {
                EditText editTextUsername = findViewById(R.id.editTextUsername);
                EditText editTextPassword = findViewById(R.id.editTextPassword);
                String username = editTextUsername.getText().toString();
                String unhashedPassword = editTextPassword.getText().toString();
                if (username.isEmpty() || unhashedPassword.isEmpty()) {
                    return null;
                }

                // TEST URL: http://localhost:8080/courtbooking/rest/UserService/user/bmaclean/202cb962ac59075b964b07152d234b70

//                final String url = "http://10.0.2.2:8080/courtbooking/rest/UserService/user/" + username + "/" + md5(unhashedPassword);
                final String url = "http://hccis.info:8080/courtbooking/rest/UserService/user/" + username + "/" + md5(unhashedPassword);
                Log.d("fgotell_TEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                User user = restTemplate.getForObject(url, User.class);
                Log.d("KGTEST", "User: " + user.toString());
                return user;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        protected void onPostExecute(User user) {
            //if the returned User is not null or invalid (type code == 0) then display the data
            if (user != null && !user.getUserTypeCode().equals("0")) {
                TextView usernameResult = findViewById(R.id.textViewUsernameResult);
                TextView firstnameResult = findViewById(R.id.textViewFirstnameResult);
                
                usernameResult.setText(user.getUsername());
                firstnameResult.setText(user.getFirstName());
            }
        }

    }

    //    https://stackoverflow.com/questions/4846484/md5-hashing-in-android
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
