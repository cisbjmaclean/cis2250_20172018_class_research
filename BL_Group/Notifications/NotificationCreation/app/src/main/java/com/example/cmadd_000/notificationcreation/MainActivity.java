/*
*   author: Christal Maddix
*   since:  March 19, 2018
*   Research Project - Android Notifications
*/

package com.example.cmadd_000.notificationcreation;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //creating a button object to start the notification process
        Button btnNotification = findViewById(R.id.btn_creation_notification);
        btnNotification.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                createNotification();
            }
        });
    }

    //creating the notification
    private void createNotification(){
        Intent intent = new Intent(this,NotificationReceiverActivity.class);

        //Pending intent is a token that you give to another application (ie. notification manager, alarm manager,
        // or other third-party application) which allows these other application to use the permissions of your
        //application to execute predefined code.
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis(), intent, 0);

        Notification notification = new Notification.Builder(this)
                //setting content and icon of notification
                .setContentTitle("Christal's Squash Court Bookings")
                .setContentText("You have notification about your bookings")
                .setSmallIcon(R.drawable.icons_chat)//find icon and put in your drawable folder
                .setContentIntent(pendingIntent)
                .build(); // when you get here the .build() becomes red if your gradle.build file min is below 16
                //go to the file and put the min to 16

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        //this will cancel the notification once it is clicked
        notification.flags = Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, notification);

    }
}
