/*
*   author: Christal Maddix
*   since:  March 19, 2018
*   Research Project - Android Notifications
*/

package com.example.cmadd_000.notificationcreation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NotificationReceiverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_receiver);
    }
}
