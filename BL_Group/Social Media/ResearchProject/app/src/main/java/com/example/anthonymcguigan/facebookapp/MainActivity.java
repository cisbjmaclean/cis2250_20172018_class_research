/*
Author: Anthony McGuigan
Date: Feb. 24, 2017

Purpose: To Allow the user to share a text to facebook.
Will hold a text box, and a button that says share it.
The share button should allow the user to share the message as a facebok message or a facebook status
 */

package com.example.anthonymcguigan.facebookapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewEnterText;
    EditText editText;
    Button buttonShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //USED LATER -- editText = (EditText)findViewById(R.id.editText);
        buttonShare = (Button)findViewById(R.id.buttonShare);

        buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText = (EditText)findViewById(R.id.editText);

                //When the button is clicked, a new intent is created
                Intent shareIntent = new Intent();
                //Specifies that the intent will send data
                shareIntent.setAction(Intent.ACTION_SEND);
                //What type of data will be sent
                shareIntent.setType("text/plain");
                //Set the share body
                String sharebody = editText.getText().toString();
                //Grab the text from the text box
                shareIntent.putExtra(Intent.EXTRA_TEXT, sharebody);

                //This will open the chooser and send the data to the chosen method
                startActivity(Intent.createChooser(shareIntent, "Share your text..."));
            }
        });
    }
}
