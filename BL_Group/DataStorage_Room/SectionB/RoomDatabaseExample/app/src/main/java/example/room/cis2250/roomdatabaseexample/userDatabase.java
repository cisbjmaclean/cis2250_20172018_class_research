package example.room.cis2250.roomdatabaseexample;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by jerem on 2018-02-04.
 */
@Database(entities = User.class, version = 1)
public abstract class userDatabase extends RoomDatabase{
    public abstract UserDao userDao();
}
