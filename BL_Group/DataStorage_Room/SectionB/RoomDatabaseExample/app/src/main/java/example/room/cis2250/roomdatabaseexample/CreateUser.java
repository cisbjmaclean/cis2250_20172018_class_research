package example.room.cis2250.roomdatabaseexample;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * Created by jerem on 2018-02-04.
 */

public class CreateUser extends AppCompatActivity {

    EditText firstName;
    EditText lastName;
    EditText email;
    Button button;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_user);
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        button = findViewById(R.id.button);

        final userDatabase db = Room.databaseBuilder(getApplicationContext(), userDatabase.class, "Production")
                .allowMainThreadQueries()
                .build();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < 10; i++) {
                    User user = new User(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString());
                    User user2 = new User(3, "John", lastName.getText().toString(), email.getText().toString());
                    db.userDao().insertAll(user);
                    db.userDao().updateAll(user2);
                    List<User> users = db.userDao().getAllUsers();
                    for(User current: users){
                        Log.d("JCTEST", current.toString());
                    }
                    Log.d("JCTEST", "Size = " + users.size());
                }
                startActivity(new Intent(CreateUser.this, MainActivity.class));
            }
        });
    }
}
