package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.media.Rating;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import static cis2250.hccis.info.fragmentapp.R.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentJason#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentJason extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RadioGroup rg;
    RadioButton rb;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentJason() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentJason.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentJason newInstance(String param1, String param2) {
        FragmentJason fragment = new FragmentJason();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {

        super.onStart();
        mListener = (OnFragmentInteractionListener) getActivity();
        RatingBar ratingBar = getView().findViewById(id.ratingBarJason);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                                   @Override
                                                   public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                                       Log.d("Jason's Test", "Rating = " + ratingBar.getRating());
                                                       mListener.onFragmentInteraction("hello from the FragmentJason. The rating has changed to " + ratingBar.getRating());

                                                       String msgText = "";
                                                       float rating = ratingBar.getRating();
                                                       if (rating < 3.0) {
                                                           msgText = "This rating is low.";
                                                       }
                                                       if (rating >= 3.0 && rating < 5) {
                                                           msgText = "This is a farily good, average rating.";
                                                       }
                                                       if (rating == 5.0) {
                                                           msgText = "This is a perfect rating! Thank you.";
                                                       }

                                                       Toast.makeText(getActivity().getBaseContext(), msgText, Toast.LENGTH_LONG).show();
                                                   }

                                               }

        );


        //create the spinner drop down values
        final Spinner spinnerDropDown, spinnerDropdown;
        final ArrayAdapter adapter;
        adapter = ArrayAdapter.createFromResource(this.getActivity(), array.mealType, android.R.layout.simple_spinner_item);
        spinnerDropDown = (Spinner) getView().findViewById(id.spinnerJason);
        spinnerDropDown.setAdapter(adapter);
        //select listener for the drop down (spinner) widget
        spinnerDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                        Toast.makeText(getActivity().getBaseContext(), "your meal preference has been added",Toast.LENGTH_LONG).show();
                                                    }
            @Override
            public void onNothingSelected(AdapterView <?> adapterView) {
            }
});

     //Make a selection for when to have a meal using radio buttons and listen for check
        RadioGroup radioGroup = (RadioGroup) getView().findViewById(id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                //go through the various buttons and set the name for messages
                String mealString = "Nothing Selected";
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case id.radioButton1:
                        mealString = "Breakfast";
                        break;
                    case id.radioButton2:
                        mealString = "Lunch";
                        break;
                    case id.radioButton3:
                        mealString = "Dinner";
                        break;
                }
                Toast.makeText(getActivity().getBaseContext(), "We will reserve a table for you at " + mealString, Toast.LENGTH_LONG).show();
                Log.d("Meal Time", "the selected meal time was for = " + mealString);
                mListener.onFragmentInteraction("the selected meal time was for = " + mealString);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(layout.fragment_jason, container, false);


    }

 /*   // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
