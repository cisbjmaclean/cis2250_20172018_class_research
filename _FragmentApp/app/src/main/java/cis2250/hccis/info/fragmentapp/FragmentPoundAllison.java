package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPoundAllison.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPoundAllison#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPoundAllison extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentPoundAllison() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPoundAllison.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPoundAllison newInstance(String param1, String param2) {
        FragmentPoundAllison fragment = new FragmentPoundAllison();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        final TextView resultText = getView().findViewById(R.id.resultTextAllison);  //result text object to hold message in app
        final TextView ratingText = getView().findViewById(R.id.ratingTextAllison); //result text object to display rating result
        RatingBar ratingBar = getView().findViewById(R.id.ratingBarAllison);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("Allison test", "rating = " + ratingBar.getRating());
                ratingText.setText("Rating: " + ratingBar.getRating());  //sets screen text area for rating result
               //mListener.onFragmentInteraction("hi from the fragment, the rating is " + ratingBar.getRating());
            }
        });
        //switch handler - AP as per assignment 1 (fragments)
        Switch allieSwitch = getView().findViewById(R.id.switchAllison);
        allieSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) { //if user selects good on the slide bar
                    resultText.setText("Awww, thank you!  User selected switch as  = " + compoundButton.isChecked() + " which means they think my fragment is good!");
                    Log.d("Switch test", "Awww, thank you!  User selected switch as  = " + compoundButton.isChecked() + " which means they think my fragment is good!");
                    //mListener.onFragmentInteraction("hi from the fragment, the switch result is " + compoundButton.isChecked());

                } else {
                    //if the user selects bad on the slide bar
                    resultText.setText("Ok, I'll try harder next time!  User selected switch as  = " + compoundButton.isChecked() + " which means they think my fragment is bad.");
                    Log.d("Switch test", "Ok, I'll try harder next time!  User selected switch as  = " + compoundButton.isChecked() + " which means they think my fragment is bad.");
                    //mListener.onFragmentInteraction("hi from the fragment, the switch result is " + compoundButton.isChecked());
                }
            }
        });
        //radio button handler - AP as per assignment 1 (fragments)
        final RadioGroup allieGroup = getView().findViewById(R.id.buttonGroupAllison);
        allieGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                String valueOfRad = ((RadioButton) getView().findViewById(allieGroup.getCheckedRadioButtonId())).getText().toString();
                Log.d("Radio test", "The result of the radiobutton is " + valueOfRad);
                switch (valueOfRad.toLowerCase()) {
                    case "yes":
                        resultText.setText("Aw, thank you very much.  I doubt that's true but I appreciate you saying so.");
                        Log.d("Radio result", "Aw, thank you very much.  I doubt that's true but I appreciate you saying so.");
                        break;
                    case "no":
                        resultText.setText("That's ok, we can't all be the favorite!");
                        Log.d("Radio result", "That's ok, we can't all be the favorite!");
                        break;
                    default:
                        resultText.setText("Something went really wrong... how did you select a third radio button?  The result of the radiobutton is " + valueOfRad);
                        Log.d("Radio test", "Something went really wrong... how did you select a third radio button?  The result of the radiobutton is " + valueOfRad);
                        break;

                }


            }


        });
        //floating action button handler, changes color on click! - AP as per assignment 1 (fragments)
        final FloatingActionButton spaButton = getView().findViewById(R.id.floatingActionButtonAllison);
        spaButton.setOnClickListener(new FloatingActionButton.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultText.setText("You clicked the spa button!  Sorry, I know... not entirely entertaining but wasn't sure how else to demonstrate the widgets functionality");
                if(spaButton.getBackgroundTintList() != getResources().getColorStateList(R.color.colorAccent)) {
                    spaButton.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccent));  //changes button background color
                }
                else{
                    spaButton.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary)); //changes button background color
                    resultText.setText("I hope this satisfied the requirements!");
                }

            }


        });

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pound_allison, container, false);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
