package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Switch;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentProudeDavid.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentProudeDavid#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProudeDavid extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentProudeDavid() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentProudeDavid.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentProudeDavid newInstance(String param1, String param2) {
        FragmentProudeDavid fragment = new FragmentProudeDavid();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_proude_david, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
*/

    @Override
    public void onStart() {
        super.onStart();
        final Switch aSwitch = getView().findViewById(R.id.switch1);
        aSwitch.setOnClickListener(new Switch.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("bjtest", "switch=" + aSwitch.getTextOn());
               // mListener.onFragmentInteraction("hello from the fragment, " + aSwitch.getTextOn());
            }
        });


final ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.progressBarDavid);
final EditText numberInput = (EditText) getView().findViewById(R.id.number_input);
Button submit = (Button) getView().findViewById(R.id.buttonSubmit);

submit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String progress = numberInput.getText().toString();
        progressBar.setProgress(Integer.parseInt(progress));
        //mListener.onFragmentInteraction("The progress is "+progressBar.getProgress()+ "% (davidProude)");
    }
});

     //   final Spinner aSpinner = getView().findViewById(R.id.spinner);
//aSpinner.setOnItemClickListener(new aSpinner.getOnItemClickListener());


        RatingBar ratingBar = getView().findViewById(R.id.ratingBarDavid);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d("bjtest", "rating="+ratingBar.getRating());
                //mListener.onFragmentInteraction("The rating is "+ratingBar.getRating()+ " (davidProude)");
            }
        });




    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
